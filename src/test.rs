use crate::{CliArgs, Runner};

use std::fs::create_dir;
use std::os::unix::fs::symlink;
use std::path::PathBuf;

type Summary = Vec<(
    std::ffi::OsString,
    std::fs::FileType,
    Option<std::path::PathBuf>,
)>; // What I'd give to write _ (or `impl PartialEq + std::fmt::Debug`) here and in the function signature

struct Fixture {
    base: tempfile::TempDir,
    farm: PathBuf,
    data: Vec<PathBuf>,
    summary: Summary,
}

fn summarize(dir: &std::path::Path) -> Summary {
    let mut observed: Vec<_> = dir.read_dir().unwrap().collect::<Result<_, _>>().unwrap();
    for i in 0.. {
        let item = match observed.get(i) {
            Some(x) => x,
            _ => break,
        };

        if item.file_type().unwrap().is_dir() {
            let sub: Vec<_> = item
                .path()
                .read_dir()
                .unwrap()
                .collect::<Result<_, _>>()
                .unwrap();
            observed.extend(sub);
        }
    }

    // Build something stably comparable
    //
    // Could be hashed, but it's small enough and not hashing eases debugging
    observed
        .iter()
        .map(|e| {
            (
                e.file_name(),
                e.file_type().unwrap(),
                std::fs::read_link(e.path()).ok(),
            )
        })
        .collect::<Vec<_>>()
}

impl Fixture {
    /// Optionally remove the farm and assert that the rest is as it was before
    fn clean_up_testing(self, expect_presence: bool) {
        if expect_presence {
            std::fs::remove_dir_all(self.farm).unwrap();
        }

        assert_eq!(summarize(self.base.path()), self.summary);
    }
}

/// Build the test case from <https://sourceforge.net/p/xstow/bugs/8>/
fn build_awefully_linked() -> Fixture {
    let base = tempfile::tempdir().expect("Failed to create base directory");

    let farm = base.path().join("farm");
    let a = base.path().join("data-a");
    let b = base.path().join("data-b");

    create_dir(&a).unwrap();
    symlink("../a-data/", a.join("common")).unwrap();

    let a_data = base.path().join("a-data");
    let a_data2 = base.path().join("a-data-2");
    create_dir(&a_data).unwrap();
    symlink("../a-data-2/", a_data.join("common-2")).unwrap();
    create_dir(&a_data2).unwrap();
    drop(std::fs::File::create(a_data2.join("from-a")).unwrap());

    create_dir(&b).unwrap();
    let b_common = b.join("common");
    create_dir(&b_common).unwrap();
    let b_common2 = b_common.join("common-2");
    create_dir(&b_common2).unwrap();
    drop(std::fs::File::create(b_common2.join("from-b")).unwrap());

    let summary = summarize(base.path());

    Fixture {
        base,
        farm,
        data: vec![a, b],
        summary,
    }
}

/// A simple test case with >2 data directories
fn build_triple() -> Fixture {
    let base = tempfile::tempdir().expect("Failed to create base directory");

    let farm = base.path().join("farm");
    let a = base.path().join("data-a");
    let b = base.path().join("data-b");
    let c = base.path().join("data-c/");

    create_dir(&a).unwrap();
    create_dir(&b).unwrap();
    create_dir(&c).unwrap();

    drop(std::fs::File::create(a.join("from-a")).unwrap());
    drop(std::fs::File::create(b.join("from-b")).unwrap());

    let a_sub_common = a.join("sub-ab");
    create_dir(&a_sub_common).unwrap();
    drop(std::fs::File::create(a_sub_common.join("from-a")).unwrap());
    let b_sub_common = b.join("sub-ab");
    create_dir(&b_sub_common).unwrap();
    drop(std::fs::File::create(b_sub_common.join("from-b")).unwrap());

    let c_sub = c.join("sub-c");
    create_dir(c_sub).unwrap();

    let summary = summarize(base.path());

    Fixture {
        base,
        farm,
        data: vec![a, b, c],
        summary,
    }
}

#[test]
fn test_awefully_linked() {
    let fixture = build_awefully_linked();

    let from_a = fixture.farm.join("common/common-2/from-a");
    let from_b = fixture.farm.join("common/common-2/from-b");

    let args = CliArgs {
        farm: fixture.farm.clone(),
        data: fixture.data.clone(),
        allow_empty: false,
        accept_file_presence: false,
    };

    let result = Runner::new(args).unwrap().execute();

    /*
    std::process::Command::new("tree")
        .args(&[fixture.base.path()])
        .spawn()
        .unwrap()
        .wait()
        .unwrap()
        ;

    if result.is_err() {
        std::mem::forget(fixture.base);
    }
    */

    result.unwrap();

    match std::fs::read_link(from_a) {
        Ok(ref p) if *p == PathBuf::new().join("../../../data-a/common/common-2/from-a") => (),
        x => panic!("Expected link to from-a, got {x:?}"),
    }
    match std::fs::read_link(from_b) {
        Ok(ref p) if *p == PathBuf::new().join("../../../data-b/common/common-2/from-b") => (),
        x => panic!("Expected link to from-b, got {x:?}"),
    }

    fixture.clean_up_testing(true);
}

fn test_sequence(fixture: Fixture) {
    // Single source should give single symlink...

    let args = CliArgs {
        farm: fixture.farm.clone(),
        data: fixture.data[..1].to_vec(),
        allow_empty: false,
        accept_file_presence: false,
    };
    Runner::new(args).unwrap().execute().unwrap();

    match std::fs::read_link(&fixture.farm) {
        Ok(ref p) if *p == PathBuf::new().join("data-a") => (),
        x => panic!("Expected link to data-a, got {x:?}"),
    }

    // ... while all sources should give a nested structure ...

    let args = CliArgs {
        farm: fixture.farm.clone(),
        data: fixture.data.clone(),
        allow_empty: false,
        accept_file_presence: false,
    };
    Runner::new(args).unwrap().execute().unwrap();

    assert!(fixture
        .farm
        .symlink_metadata()
        .unwrap()
        .file_type()
        .is_dir());

    // ... and empty list should remove it again.

    let args = CliArgs {
        farm: fixture.farm.clone(),
        data: vec![],
        allow_empty: true,
        accept_file_presence: false,
    };
    Runner::new(args).unwrap().execute().unwrap();

    assert!(fixture.farm.symlink_metadata().unwrap_err().kind() == std::io::ErrorKind::NotFound);

    fixture.clean_up_testing(false);
}

#[test]
fn test_sequence_awefullylinked() {
    test_sequence(build_awefully_linked());
}

#[test]
fn test_sequence_triple() {
    test_sequence(build_triple());
}
