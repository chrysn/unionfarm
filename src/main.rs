use std::collections::HashMap;
use std::fs::FileType;
use std::path::{Path, PathBuf};

// As recommended by cli-wg's tutorial
use clap::Parser;
use exitfailure::ExitFailure;
use failure::ResultExt as _;

use pathdiff::diff_paths;

use log::{debug, info, warn};

#[cfg(test)]
mod test;

#[derive(Debug)]
struct Error(String);

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl std::error::Error for Error {}

#[derive(Parser, Debug)]
struct CliArgs {
    farm: PathBuf,
    data: Vec<PathBuf>,
    /// Accept the absence of data directories
    ///
    /// Without this option, an empty list of data directories is not accepted for safety reasons;
    /// with this option set and no data directories given, the farm will be removed completely.
    /// (Non-symlink files and symlinks that don't point to their own names will still constitute
    /// an error and not be removed).
    #[arg(long = "allow-empty")]
    allow_empty: bool,
    /// Accept files that are present in the farm and do not stem from any data directory
    ///
    /// Setting this option will leave actual files in place as well as empty directories and
    /// symlinks that do not look like they originate from a previous unionfarm run. Symlinks whose
    /// targets end with the link's name relative to the farm are still removed.
    #[arg(long = "accept-file-presence")]
    accept_file_presence: bool,
}

#[derive(Parser, Debug)]
#[clap(
    about = "Create or update a symlink farm from the given data sources",
    version
)]
struct CliArgsWithVerbosity {
    #[command(flatten)]
    program_args: CliArgs,
    #[command(flatten)]
    verbose: clap_verbosity_flag::Verbosity<clap_verbosity_flag::WarnLevel>,
}

impl CliArgsWithVerbosity {
    fn setup_log(self) -> Result<CliArgs, ExitFailure> {
        let level = self.verbose.log_level().unwrap_or(log::Level::Error);
        pretty_env_logger::formatted_builder()
            .filter(None, level.to_level_filter())
            .try_init()?;

        debug!("Verbosity set to {}", level);

        Ok(self.program_args)
    }
}

struct Runner {
    args: CliArgs,
    tasks: Vec<PreparedEntry>,
    data_templates: Vec<PathBuf>,
}

/// Like a relative path from farm and data, but pre-populated with information about the presence
/// of the farm and data files, and their type as collected at listing time.
///
/// Most practical methods sit on `BoundPreparedEntry`.
#[derive(Debug)]
struct PreparedEntry {
    relative: PathBuf,
    /// The type of the file relative to the farm location, if one exists. This is used as an
    /// "lstat" type, ie. it always reflects the type of the file when symlinks are not
    /// dereferenced (because it is used to decide whether a single symlink should be updated to a
    /// directory or a directory to a symlink).
    farm_presence: Option<FileType>,
    /// The respective types of the files relative to the data locations, if they exist. This is
    /// used as a "non-lstat" type, as the relevant question is "can we recurse this to merge a
    /// directory". There may have a Symlink value temporarily when data is only available from the
    /// directory listing stage, but they are replaced with the proper values in `.upgrade()`.
    data_presence: Vec<Option<FileType>>, // FIXME: It Would Be Nice if Vec<Self> could contain all data.
}

use core::fmt;

impl fmt::Display for PreparedEntry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "entry at relative {:?} ({:?} in farm, {:?} in data)",
            self.relative, self.farm_presence, self.data_presence
        )
    }
}

/// State that a directory should have after completion of the program
#[derive(Debug, PartialEq)]
enum DesiredState {
    /// There is no item to be linked here
    Absent,
    /// There is exactly one item to be linked here
    SingleLink(usize),
    /// There are multiple items to be linked here, and all of them are directories (implemented by
    /// having a directory there and applying unionfarm there again)
    Recursion,
}

impl PreparedEntry {
    fn bind(self, args: &CliArgs) -> BoundPreparedEntry<'_> {
        BoundPreparedEntry { entry: self, args }
    }
}

/// Short-term helper that binds a `PreparedEntry` to the general arguments (that give base path
/// information to farm and data).
#[derive(Debug)]
struct BoundPreparedEntry<'a> {
    entry: PreparedEntry,
    args: &'a CliArgs,
}

impl fmt::Display for BoundPreparedEntry<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.entry.fmt(f)
    }
}

impl BoundPreparedEntry<'_> {
    /// Upgrade data file types by following symlinks
    ///
    /// The file type of the farm reference is not updated, as it used as a lstat type (see struct
    /// documentation).
    // Could instead be done lazily in desired_state and similar, but the only performace benefit
    // this'd give is for entries present in only one data source (quite common), that source would
    // not need to be be deref'd (single syscall possibly hitting disk) if it is a symlink (rare
    // unless mirroring a symlink farm).
    fn upgrade(&mut self) -> Result<(), ExitFailure> {
        let relative = &self.entry.relative;
        for (type_, base) in self
            .entry
            .data_presence
            .iter_mut()
            .zip(self.args.data.iter())
        {
            if type_.map(|x| x.is_symlink()) == Some(true) {
                let full_entry = base.join(relative);
                *type_ = Some(full_entry
                    .metadata()
                    .or_else(|_| {
                        // FIXME: defer this warning until there actually arises a situation where
                        // it matters
                        warn!("Ignoring metadata error at {:?} inside {:?}, leaving it as a broken symlink", relative, base);
                        full_entry.symlink_metadata()
                    })
                    .map(|x| x.file_type())
                    .with_context(|_| format!("Inspecting metadata of {relative:?} at {base:?}"))?
                    );
            }
        }

        Ok(())
    }

    // Could be made to take &mut self and lazy upgrading, see .upgrade
    fn desired_state(&self) -> Result<DesiredState, ExitFailure> {
        let mut the_index = None;
        let mut files = 0;
        let mut directories = 0;
        for (i, d) in self.entry.data_presence.iter().enumerate() {
            if let Some(d) = d {
                if d.is_file()
                    // a broken one, given it was not upgraded
                    || d.is_symlink()
                {
                    files += 1;
                } else if d.is_dir() {
                    directories += 1;
                } else {
                    // `upgrade()` should have ensured that symlinks are resolved, and there should
                    // not be a third state other than symlink, file or directory.
                    panic!(
                        "Unknown state of {:?} at {:?}: {:?}",
                        self.entry.relative, self.args.data[i], d
                    );
                }
                the_index = Some(i);
            }
        }

        let result = if files + directories == 0 {
            DesiredState::Absent
        } else if files + directories == 1 {
            DesiredState::SingleLink(
                the_index.expect("index not set when directory or file discovered"),
            )
        } else if files == 0 {
            DesiredState::Recursion
        } else {
            return Err(Error(format!("Found {} file(s) and {} directorie(s) in {:?}; unionfarm can not process multiple files with the same name (or files sharing a name with a directory)", files, directories, self.entry.relative)).into());
        };

        debug!(
            "Deciding fate of {:?}, found {} file(s) (or broken symlinks) and {} directories: {:?}",
            self.entry.relative, files, directories, result
        );

        Ok(result)
    }
}

/// Given a `PathBuf` whose symlink-metadata is fetched, return its `FileType`, accept absence (by
/// returning Ok(None)) and pass any other error out in a user-readable form.
//
// (P as in std::fs::symlink_metadata plus resonable expectations)
fn get_filetype<P>(p: &P) -> Result<Option<FileType>, ExitFailure>
where
    P: AsRef<std::path::Path> + std::fmt::Debug,
{
    use failure::Fail as _;

    match std::fs::symlink_metadata(p) {
        Ok(m) => Ok(Some(m.file_type())),
        Err(ref e) if e.kind() == std::io::ErrorKind::NotFound => Ok(None),
        Err(e) => Err(e.context(format!("Error accessing {p:?}")).into()),
    }
}

/// Like base.join(relative), but don't make the result end with a slash just because relative is
/// empty.
///
/// This is desirable behavior when dealing with symlinks, as a symlink can only be created
/// (symlink("foo", ...) as opposed to symlink("foo/", ..)) or removed (unlink("foo/") sees the
/// linked directory) when no such slash is present.
///
/// In this program, that this case happens when creating or removing a symlink that is the farm
/// itself.
fn notrailjoin<P1, P2>(base: P1, relative: P2) -> PathBuf
where
    P1: AsRef<std::path::Path>,
    P2: AsRef<std::path::Path>,
{
    let relative = relative.as_ref();
    let base = base.as_ref();

    if relative == std::path::Path::new("") {
        base.to_path_buf()
    } else {
        base.join(relative)
    }
}

impl Runner {
    fn new(args: CliArgs) -> Result<Self, ExitFailure> {
        // data template paths point from the directory *containing* the farm because
        // a) the inversion of the relative path will bring us there, and
        // b) if there is only one data folder, the farm will point to that and won't need a '..'
        //    in there either
        let farmparent = args.farm.parent().ok_or_else(||
            // On Unix that'd be really dangerous, on Windows it might make sense to make a
            // complete drive as symlink farm, but I can't really test it, so no further effort is
            // made here.
            Error("Farm has no parent, that is not supported".into()))?;

        let data_templates = args
            .data
            .iter()
            .map(|p| {
                diff_paths(p, farmparent)
                    .ok_or_else(|| Error("Data paths must be absolute if farm is absolute".into()))
            })
            .collect::<Result<_, _>>()?;

        Ok(Runner {
            args,
            tasks: vec![],
            data_templates,
        })
    }

    fn check_arguments(&self) -> Result<(), ExitFailure> {
        if self.args.data.is_empty() && !self.args.allow_empty {
            return Err(Error("No data directories given. (Pass --allow-empty to actually remove the complete farm).".into()).into());
        }

        // Not printing that at new() stage b/c logging is not set up there yet
        debug!(
            "Symlinks will contain those references: {:?}",
            self.data_templates
        );

        Ok(())
    }

    fn execute(&mut self) -> Result<(), ExitFailure> {
        self.check_arguments()?;

        let initial = PreparedEntry {
            relative: "".into(),
            farm_presence: get_filetype(&self.args.farm)?,
            data_presence: self
                .args
                .data
                .iter()
                // Here we could potentially save a syscall in .upgrade() later if we made
                // get_filetype fetch metadata, but above in farm_presence, we need
                // symlink_metadata.
                .map(get_filetype)
                .collect::<Result<_, _>>()?,
        };
        self.tasks.push(initial);

        while let Some(item) = self.tasks.pop() {
            self.set_symlink_or_recurse(item)?;
        }

        Ok(())
    }

    /// Make farm a synlink to the single data item if there is only one, or a directory and start the
    /// process of creating its items.
    fn set_symlink_or_recurse(&mut self, subpath: PreparedEntry) -> Result<(), ExitFailure> {
        let mut subpath = subpath.bind(&self.args);

        // Not checking if entry.farm_type.is_file(): that'd hit a clear_farm_at one way or another
        // anyway, and raise a unified error there

        subpath.upgrade()?;

        let decision = subpath.desired_state()?;

        if (decision == DesiredState::Absent && subpath.entry.farm_presence.is_some())
            || (decision == DesiredState::Recursion &&
            // present but not a directory
            subpath.entry.farm_presence.map(|t| t.is_dir()) == Some(false))
        {
            self.clear_farm_at(&subpath.entry.relative)?;
        }

        let full_farm = notrailjoin(&self.args.farm, &subpath.entry.relative);

        match decision {
            DesiredState::SingleLink(i) => {
                let relative_part = &self.data_templates[i];
                let mut desired_link = relative_part.join(&subpath.entry.relative);

                if relative_part.is_relative() {
                    desired_link =
                        pathdiff::diff_paths(std::path::Path::new(""), &subpath.entry.relative)
                            .expect("Building a ../.. version of foo/bar should always be possible")
                            .join(&desired_link);
                }

                debug!(
                    "Symlink at {:?} should point to {:?}",
                    subpath.entry.relative, desired_link
                );

                let rel = &subpath.entry.relative;
                if subpath.entry.farm_presence.map(|p| p.is_symlink()) == Some(true)
                    && desired_link
                        == std::fs::read_link(&full_farm)
                            .with_context(|_| format!("Reading link {:?} at farm", &rel))?
                {
                    info!("Leaving symlink in place at {:?}", rel);
                    return Ok(());
                }

                if subpath.entry.farm_presence.is_some() {
                    debug!(
                        "Late clearing out existing undesired data at {:?}",
                        &subpath.entry.relative
                    );
                    // Couldn't clear them out in the initial clear-out stage when built relative
                    // part was not available yet
                    self.clear_farm_at(&subpath.entry.relative)?;
                }

                info!("Creating new symlink at {:?}", subpath.entry.relative);
                std::os::unix::fs::symlink(desired_link, full_farm).with_context(|_| {
                    format!("Creating symlink at {:?}", subpath.entry.relative)
                })?;
            }
            DesiredState::Recursion => {
                if subpath.entry.farm_presence.map(|p| p.is_dir()) != Some(true) {
                    info!("Creating directory for recursion at {:?}", full_farm);
                    std::fs::create_dir(full_farm).with_context(|_| {
                        format!("When creating {:?} at the farm", subpath.entry.relative)
                    })?;
                }
                // Destroying in two steps to avoid the issue of https://github.com/rust-lang/rust/issues/59159
                // (This frees the &self contained in subpath).
                let subpath = subpath.entry;
                self.enqueue_subitems(subpath)?;
            }
            DesiredState::Absent => (),
        }

        Ok(())
    }

    /// Remove a file, directory or symlink if it is safe to remove
    fn clear_farm_at(&self, relative_path: &Path) -> Result<(), ExitFailure> {
        info!("Clearing out recursively at {:?}", relative_path);

        struct ItemForRemoval {
            path: PathBuf,
            type_: Option<FileType>,
        }
        let mut stack = vec![ItemForRemoval {
            path: relative_path.to_path_buf(),
            type_: None,
        }];

        while let Some(ItemForRemoval {
            path: last,
            type_: lasttype,
        }) = stack.last()
        {
            let last_full = notrailjoin(&self.args.farm, last);

            let lasttype = match lasttype {
                Some(x) => *x,
                None => last_full
                    .symlink_metadata()
                    .with_context(|_| format!("Inspecting to-be-deleted {last:?}"))?
                    .file_type(),
            };

            if lasttype.is_dir() {
                if self.args.accept_file_presence {
                    warn!("Not attempting to remove directory {:?}", last);
                    stack.pop();
                } else {
                    let old_stacklen = stack.len();

                    // FIXME build extend data in-place
                    let items: Vec<_> = last_full
                        .read_dir()
                        .with_context(|_| format!("Reading to-be-deleted {last:?}"))?
                        .map(|i| {
                            i.map(|de| ItemForRemoval {
                                path: last.join(de.file_name()),
                                type_: de.file_type().ok(),
                            })
                            .with_context(|_| format!("Reading item in to-be-deleted {last:?}"))
                        })
                        .collect::<Result<_, _>>()?;

                    // clone: Variable kept around for error and debug messages even though we mess
                    // with its source
                    let last = last.clone();
                    stack.extend(items);

                    if stack.len() == old_stacklen {
                        stack.pop();

                        debug!("Directory {:?} is empty (now?), removing.", last);
                        std::fs::remove_dir(&last_full).with_context(|_| {
                            format!("Removing empty directory {last:?} from farm")
                        })?;
                    }
                }
            } else if lasttype.is_symlink() {
                let target = std::fs::read_link(&last_full)
                    .with_context(|_| format!("Reading link at {last:?}"))?;
                if target.ends_with(last) {
                    debug!("Link at {:?} looks like a farmed link, removing", last);
                    std::fs::remove_file(&last_full)
                        .with_context(|_| format!("Removing symlink at {last:?}"))?;
                } else {
                    // It's none of ours symlinks

                    if self.args.accept_file_presence {
                        warn!("Leaving symlink at {:?} in place", &last);
                        stack.pop();
                    } else {
                        return Err(
                            Error(format!("Unexpected symlink at {:?} in farm", &last)).into()
                        );
                    }
                }

                stack.pop();
            } else {
                // It's an actual file

                if self.args.accept_file_presence {
                    warn!("Leaving file at {:?} in place", &last);
                    stack.pop();
                } else {
                    return Err(Error(format!("Unexpected file at {:?} in farm", &last)).into());
                }
            }
        }

        Ok(())
    }

    /// Schedule an update of a farm directory that spools to all the data items; err out if any of
    /// the corresponding data items is not a directory (or whatever is configured)
    ///
    /// It is the caller's responsibility to ensure that the farm directory exists and is not a
    /// previous run's symlink.
    fn enqueue_subitems(&mut self, subpath: PreparedEntry) -> Result<(), ExitFailure> {
        debug!(
            "Iterating through farm and data dirs to enqueue new work items inside {:?}",
            subpath.relative
        );

        let full_farm = self.args.farm.join(&subpath.relative);

        // Starting the HashMap from the farm should mean the littlest re-allocation in the common
        // case where all entries are already present
        let mut existing: HashMap<_, _> = full_farm
            .read_dir()
            .with_context(|_| format!("Failed to open farm at {:?}", subpath.relative))?
            .map(|i| {
                i.map(|d| {
                    (
                        d.file_name(),
                        PreparedEntry {
                            relative: subpath.relative.join(d.file_name()),
                            farm_presence: Some(
                                d.file_type()
                                    .expect("Directory items are expected to have some type"),
                            ),
                            data_presence: vec![None; self.args.data.len()],
                        },
                    )
                })
            })
            .collect::<Result<_, std::io::Error>>()
            .with_context(|_| format!("Failed to read farm at {:?}", subpath.relative))?;

        for (i, base) in self.args.data.iter().enumerate() {
            if subpath.data_presence[i].is_none() {
                // Happens if more than two data directories are present and have different sets of
                // directories; don't try to list an absent one
                continue;
            }

            for dirent in base
                .join(&subpath.relative)
                .read_dir()
                .with_context(|_| format!("Failed to open {:?} at {:?}", base, subpath.relative))?
            {
                let dirent = dirent.with_context(|_| {
                    format!("Failed to read {:?} at {:?}", base, subpath.relative)
                })?;

                let extent = existing
                    .entry(dirent.file_name())
                    .or_insert_with(|| PreparedEntry {
                        relative: subpath.relative.join(dirent.file_name()),
                        farm_presence: None,
                        data_presence: vec![None; self.args.data.len()],
                    });
                extent.data_presence[i] = Some(
                    dirent
                        .file_type()
                        .expect("Directory items are expected to have some type"),
                );
            }
        }

        self.tasks.extend(existing.drain().map(|(_, v)| v));

        Ok(())
    }
}

fn main() -> Result<(), ExitFailure> {
    Runner::new(CliArgsWithVerbosity::parse().setup_log()?)?.execute()
}
